\documentclass{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{rose} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{etex}

\usepackage{amsmath,amsfonts,amssymb,amsthm,mathtools} % Most people will need these for mathematics

%Figures
\usepackage{graphicx}	%Graphics package
\usepackage[center]{caption}
\usepackage{subcaption}
\usepackage{wrapfig}  % Wrap text around figures	
\usepackage{float} % To place figures correctly

\usepackage{makeidx} % Probably important.
\usepackage{listings} % To include code-snippets
\usepackage{color}
\usepackage{hyperref} % Clickable links to urls, internal and external references etc.

\lstset{
    basicstyle=\footnotesize\ttfamily,
  identifierstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  keywordstyle=\color{blue},
  stringstyle=\color{orange},
}


\usepackage[authoryear,square]{natbib} % References/bibliography package
\usepackage{lipsum}  % Make dummy text


% Table packages
\usepackage{tabularx}
\usepackage{array}
\usepackage{booktabs}
\newcommand*\rot{\rotatebox{90}}

\usepackage{changepage}
\usepackage{verbatim}
\usepackage{cprotect}
\usepackage{cancel}


%Special in-document vector graphics:
\usepackage{tikz}
\usetikzlibrary{shapes.geometric,calc,positioning,3d,intersections,arrows}
\usetikzlibrary{decorations.markings}
\usepackage{pgfplots}


% Custom characters
\newcommand{\slfrac}[2]{\left.#1\middle/#2\right.} % Long on-line fraction-slash: \slfrac
\newcommand{\degree}{\ensuremath{^\circ}} % The 'degree' character : \degree
\DeclareRobustCommand{\orderof}{\ensuremath{\mathcal{O}}} % The 'Order of' character: \orderof






\title[Pragmatic \LaTeX]{\LaTeX -- A pragmatic approach to typesetting your thesis}
\author{Tarjei Antonsen}
\institute{University of Troms\o}
\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% Uncomment these lines for an automatically generated outline.
%\begin{frame}{Outline}
%  \tableofcontents
%\end{frame}




\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}

%Frame%
\begin{frame}{Introduction -- Preliminaries}
\begin{itemize}
  \item The preamble
  \item The \TeX-document
  \item Dispositions
\end{itemize}

\vskip 1cm

\begin{block}{Protip}
Be careful with poshing up your thesis too much. Try too keep the style clean, classic and tasteful!
\end{block}

\end{frame}

%Frame%
\subsection{The preamble}
\begin{frame}[fragile]{The preamble}
\begin{minipage}{0.42\textwidth}
    \begin{itemize}
        \item Where the document format  and global packages and commands are imported and defined.
        \item Everything before '\verb|\begin{document}|'.
        \item Custom commands may be defined here.
    \end{itemize}
\end{minipage}
\begin{minipage}{0.55\textwidth}
    \begin{lstlisting}[language={[LATEX]TeX},frame=single]
%Document format
\documentclass[11pt, a4paper,
    twoside, titlepage]{book}
\usepackage[latin1]{inputenc}
%Maths
\usepackage{amsmath,
amsfonts,
amssymb,
amsthm,mathtools}

%Figures
\usepackage{graphicx}%Graphics
\usepackage[center]{caption}
\usepackage{subcaption}
\usepackage{wrapfig} 	
\usepackage{float}
. . .
\end{lstlisting}
\end{minipage}
\end{frame}


\subsection{The main \TeX-document}
\begin{frame}[fragile]{The main \TeX-document}
\begin{minipage}{0.55\textwidth}
    \begin{itemize}
        \item The .tex-file which contains 'everything', and most importantly your thesis.
        \item Text can be written directly here or imported with the '\verb|\include{}|'-command.
        \item The hierarchy shown here should be employed:
    \end{itemize}
\end{minipage}
\begin{minipage}{0.43\textwidth}
    \begin{lstlisting}[language={[LATEX]TeX},frame=single]
    \chapter{...}
    \section{...}
    \subsection{...}
    \subsubsection{...}
    %Unnumbered:
    \subsection*{...}
\end{lstlisting}
\end{minipage}
\end{frame}


\subsection{Disposition}

%Frame%
\begin{frame}{Disposition of thesis I}
\begin{description}
  \item[I] \hfill \\
  \textbf{Introduction}. Breifly describe the history and current state of knowledge of your field. Motivate your study and describe your angle of attack. Mention important results. Finally, give a summary of the outline of the thesis. 
  \item[II] \hfill \\
  \textbf{Background}. Extensively describe the components involved in your thesis; both general and on a more basic level. This must be well referenced and backed up by the literature. Try to include as many new studies as possible, as it shows that you are on par with the current knowledge.
  \item[III] \hfill \\
  \textbf{Methods}. Your approach to the problem. You may describe how your methods differ from other approaches here.
\end{description}
\end{frame}

\begin{frame}{Disposition of thesis II}
\begin{description}
  \item[IV] \hfill \\
  \textbf{Theoretical model}. If you have developed or you are using a theoretical model, e.g. in simulations or other considerations, discuss them here. For purely theoretical studies, this is your most important chapter and it will normally overlap with the methodology description. Derivations may be presented here.
  \item[V] \hfill \\
  \textbf{Results}. Present and thoroughly describe your findings.
  \item[VI] \hfill \\
   \textbf{Discussion}. Discuss your findings. Assess the successfulness of your approach. Discuss how your work contributes to the current literature.
  \item[VII] \hfill \\
  \textbf{Conclusions} 
\end{description}
\end{frame}

\begin{frame}
\frametitle{ \\ \centerline{\textbf{\huge Problem}}}
\begin{minipage}{0.52\textwidth}
    Import a package which produces dummy text, and use this to produce a few paragraphs. \\ 
    
    Vary the style and size of the text.
    \end{minipage}
\begin{minipage}{0.45\textwidth}
\emph{\tiny \lipsum[1]}
\end{minipage}
\end{frame}

% New section

\section{Referencing}
\begin{frame}{Referencing}
\begin{itemize}
\item Backing up your work with references to other peer reviewed studies is the most important step in ensuring its credibility.
\item Internal referencing and citing external works will be presented here.
\end{itemize}

\begin{figure}
\includegraphics[width=0.7\textwidth]{Figures/bibentry_example}
\caption{\label{fig:bibentry}Example of a properly formatted entry in the bibliography.}
\end{figure}

\end{frame}

%New frame
\subsection{Internal references}
\begin{frame}[fragile]{Internal references}
\begin{minipage}{0.48\textwidth}
    \begin{itemize}
\item All internal references are named with the \textbf{label}-command.
\item Practical when summarizing what e.g. a chapter discusses.
\item Equations are cited differently than figures and sections etc.!
\end{itemize}

\end{minipage}
\begin{minipage}{0.49\textwidth}
\begin{lstlisting}[language={[LATEX]TeX},frame=single]
%Label:
\label{REF1}

%Figures, Chpts, tabs etc.
\ref{REF1}

%Equations
\eqref{REF1}
\end{lstlisting}
\end{minipage}
\end{frame}


%New frame
\subsection{A proper bibliography}
\begin{frame}[fragile]{A proper bibliography -- Getting started}
\begin{minipage}{0.53\textwidth}
    \begin{itemize}
\item We combine \textbf{natbib} with \textbf{BibTeX} to get the best result. A few prerequisites to handle this are merely a few lines of code.
\item This combination is highly customizable, and a custom style for a typical thesis is made for this course.
\item To compile: $\LaTeX \rightarrow BibTeX\rightarrow \LaTeX\ \rightarrow \LaTeX$
\end{itemize}

\end{minipage}
\begin{minipage}{0.45\textwidth}
    \begin{lstlisting}[language={[LATEX]TeX},frame=single]
%In the preamble:
\usepackage[authoryear,
square]{natbib}

%Before \end{document}
\bibliographystyle{
Template_LastNameFirst...
_Alphabetical}

\bibliography
{database_template}
\end{lstlisting}
\end{minipage}
\end{frame}



%New Frame
\begin{frame}[fragile]{A proper bibliography -- Style and preferences}
\begin{itemize}
\item The style of citations is specified in the preamble, as shown previously. The style of the bibliography can be a bit trickier. To avoid using time on this issue, use the customized style made for this course named ''\verb|Template_LastNameFirst_Alphabetical.bst|''. This will sort the entries alphabetically after the last name of the first author.
\end{itemize}
 \begin{lstlisting}[language={[LATEX]TeX},frame=single]
%Add right before \end{document}:
\bibliographystyle{.../Template_LastNameFirst_Alphabetical}
\bibliography{.../database_template}
\end{lstlisting}
\end{frame}


%New Frame
\begin{frame}[containsverbatim]{A proper bibliography -- The database file}

\begin{itemize}
\item All references are stored in a .bib file (a text file). The simplest modification of this file is to paste new entries directly into it. Special software may also be used for this purpose. 
\item An example file named ''\verb|database_template.bib|'' is included here.
\item Some common reference types are 'article', 'phdthesis', 'inproceedings' and 'book'.
\end{itemize}

\begin{block}{Protip}
Where possible, always use the primary source in your references. Try to avoid using secondary and tertiary sources; webpages, 'references therein' etc.
\end{block}
\end{frame}





%New Frame
\begin{frame}[fragile]{A proper bibliography -- A database entry}
\begin{lstlisting}[language={[LATEX]TeX},frame=single]
@article {havnes96,
author = {Havnes, O. and Tr{\o}im, J. and Blix,
       T. and Mortensen, W. and N{\ae}sheim,
       L. I. and Thrane, E. and T{\o}nnesen, T.},
title = {First detection of charged dust 
particles in the {E}arth's mesosphere},
journal = {Journal of Geophysical Research:
       Space Physics},
volume = {101},
number = {A5},
issn = {2156-2202},
doi = {10.1029/96JA00003},
pages = {10839--10847},
year = {1996},
}
\end{lstlisting}
\end{frame}

%New Frame
\begin{frame}[containsverbatim]{A proper bibliography -- Using the database}
\begin{itemize}
\item There are two main ways to cite a work with \textbf{natbib}:
\begin{itemize}
\item \verb|\citet{...}| produces the normal format, i.e. Someone et al. (2008).
\item \verb|\citep{...}| produces the citation in parenthesis, i.e. (Someone et al., 2008).
\end{itemize}
\item Other citation commands can be found in the online documentation.
\end{itemize}

\begin{block}{Protip}
A typical bibliography entry is at least a couple of hundred characters long. In other words, you will save a lot of time importing the references from the publisher online as compared to manually punching them into you database.
\end{block}
\end{frame}




% New frame
\begin{frame}
\frametitle{ \\ \centerline{\textbf{\huge Problem}}}
\begin{columns}[c]
\column{2.0in}
Find the article '\emph{Broken Symmetries and the Masses of Gauge Bosons}' online, export the citation and use it with different styles!
\column{2.3in}
\framebox{\includegraphics[width=2.3in]{Figures/higgs}}
\end{columns}
\end{frame}




%New section

\section{Typesetting plain text and equations}
\begin{frame}{Typesetting plain text and equations}
The main treatment of these subjects is left to the compendium.
\vspace{2cm}
\begin{block}{Protip}
Always look for macros or packages to simplify the typesetting of text or maths you use a lot. Packages made especially for typesetting e.g. quantum physics exist.
\end{block}
\end{frame}


% New frame
\subsection{Plain text}
\begin{frame}[fragile]{Plain text}
\begin{minipage}{0.50\textwidth}
\begin{itemize}
\item The main concern in text formatting with \LaTeX, is the style of the text it self (weight, font, size etc.). Some common commands are given in the box on the right.
\item The layout takes care of itself (most often). 
\item WikiBook entry: \url{http://en.wikibooks.org/wiki/LaTeX/Text_Formatting}.
\end{itemize}

\end{minipage}
\begin{minipage}{0.45\textwidth}
    \begin{lstlisting}[language={[LATEX]TeX},frame=single]
%Bold (Text/Math-mode):
\textbf{} / \mathbf{}
%Emphasized
\emph{}
%Suppress indent
\noindent
%Local text size
{\tiny ...} {\Huge} etc.
%Underlined
\underline{...}
%Arrow vector (Math-m.)
$\vec{...}$
\end{lstlisting}
\end{minipage}
\end{frame}

% New frame
\subsection{Equations}
\begin{frame}{Equations}

\begin{itemize}
\item General equation typesetting is handled in the compendium with an extended example; check it out.
\item As one gains experience, the time spent on typesetting equations will decay exponentially. 
\item \emph{Mathematica} can save you a lot of time, because of its fast shortcuts.
\item Useful WikiBook entry: \url{http://en.wikibooks.org/wiki/LaTeX/Mathematics}.
\end{itemize}


\begin{block}{Protip}
Vectors are defined by default with arrows. If you want bold vectors, a custom macro or package must be used.
\end{block}
\end{frame}


% New frame
\begin{frame}
\frametitle{ \\ \centerline{\textbf{\huge Problem}}}


Typeset the following equations:
\begin{equation}
\underbrace{ \pi r_d^2n_gv_{th,g}\cdot k_B T_g}_{P_N} =\frac{4\pi}{3}\rho_d r_d^3c_p\frac{\mathrm{d}T_d}{\mathrm{d}t} + L_d \frac{\mathrm{d} m_d}{\mathrm{d}t}
\end{equation}

\begin{equation}
 \slfrac{\frac{\mathrm{d}T_d}{\mathrm{d}t} = \left(\hat{P}-L_d \frac{\mathrm{d}m_d}{\mathrm{d}t}\right)}{\frac{4\pi}{3}\rho_d r_d^3 c_p}
\end{equation}

\begin{align}
\vec{F}_{drag} &= \pi r_d^2 m_g n_g v_{th,g}(\vec{v}_g-\vec{v}_d)\frac{1}{u}\Biggl\{\frac{1}{\sqrt{\pi}}\left(u + \frac{1}{2u}\right)\text{exp}(-u^2)\nonumber \\
&+ \left(1+u^2-\frac{1}{4u^2}\right) \text{erf}(u)\Biggr\}
\end{align}
\end{frame}






%New section

\section{Figures and Tables}



\begin{frame}{Figures and Tables}
\begin{itemize}
\item Like in equation typesetting, the environments for figures and tables are many.
\item We treat the topic with help from the compendium. 
\item Floats and format are topics to note!
\item Matlab can be used to export decent looking figures.
\end{itemize}
\end{frame}

\subsection{Figures}
%New frame
\begin{frame}[fragile]{Figures -- A typical entry}
The commands \verb|\usepackage{graphics}| and \verb|\usepackage{float}| should be specified in the preamble. The following command set typesets a figure:
\vspace{1cm}
\begin{lstlisting}[language={[LATEX]TeX},frame=single]
\begin{figure}
\includegraphics[width=\textwidth]{.../filename}
\caption{\label{fig:your-figure}Caption goes here.}
\end{figure}
\end{lstlisting}
\end{frame}


%New frame
\subsection{Tables}
\begin{frame}[containsverbatim]{Tables}
\begin{itemize}
\item The placement of a table is identical to figure placement. Note that the caption should be above.
\item Use the \verb|tabular|-environment. 
\item Matlab can be also be used to export tables.
\item Tables can contain graphical objects.
\end{itemize}
\end{frame}

\subsection{TikZ}
\begin{frame}{TikZ}
\begin{itemize}
\item TikZ is an option to make in document vector graphics.
\item Super nice and often super frustrating.
\item Makes your figures fitting for even the most prestigious journals, if used correctly.
\item See the compendium for a few examples. 
\end{itemize}

\begin{block}{Protip}
Using TikZ will most likely cause death.
\end{block}
\end{frame}

\begin{frame}
\frametitle{ \\ \centerline{\textbf{\huge Problem}}}

\begin{columns}[c]
\column{1.5in}
Make a table, import an image and typeset them corresponding to proper formatting.
\column{1.5in}
\framebox{\includegraphics[width=1.5in]{Figures/figure_wrapped}}
\end{columns}

\end{frame}




% Commands to include a figure:
%\begin{figure}
%\includegraphics[width=\textwidth]{your-figure's-file-name}
%\caption{\label{fig:your-figure}Caption goes here.}
%\end{figure}


\end{document}








\end{document}