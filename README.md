UIT-Report
==========

This report-template adopts the suggested report structure given  by the Department of Computer Science at The University of Tromsø.

## Use:

1. Just edit the report.tex and bibliography.bib file.
2. Read comments in report.tex for each section, e.g:

```latex
%%%%%%%%%%%%%%% DESCRIPTION TECHNICAL BACKGROUND %%%%%%%%%%%%%%%%%%
% Covers the topics needed to solve the problem
% – E.g. a high-level description of linked-lists and/or trees
% – “What you needed to know before solving the problem”
% Does NOT explain your solution
% – This is covered in the design and implementation sections
```

Remember to compile 2-3 times each time you updated/detected your bibliography to get it working (LaTeX bugs.....). More about this in the comments for bibliography, in the source code (report.tex).

### Requirements
* Install (Ubuntu): ```sudo apt-get install texlive-extra-utils texlive-xetex```

## Use Gummi for LaTeX:
* Install (Ubuntu): ```sudo apt-get install gummi```
* http://en.wikipedia.org/wiki/Gummi_(software)

### Gummy config?
OK! 
-> Comming.
